# Note:
# PyQt* packages is missing due to being included in the base flatpak image
# psycopg2 install required postgres installed, so skipped...
alembic
beautifulsoup4
chardet
meson-python  # needed for buidling dbus-python
scikit-build  # needed for buidling patchelf (needed for dbus-python)
patchelf  # needed for buidling dbus-python
dbus-python
distro
flask
flask-cors
lxml
Mako
platformdirs
PyICU
pymediainfo
QtAwesome
qrcode
requests
SQLAlchemy
waitress
websockets
QDarkStyle
packaging
PyMySQL
pyenchant
pysword
pytest
pytest-runner
